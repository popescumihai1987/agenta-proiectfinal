This application has been developped for the "Java Programmer" course at InfoAcademy.com
Running the GUI needs Netbeans specific swing libraries.

Every Contact has a Name, Surname, Telephone number, Date of birth and Telephone number type.
Name and Surname are 2 characters or longer.
The telephone number is 10 numeric characters long and is prefixed with 07, 02 or 03.
The Telephone number type is automatically set after checing the telephone number prefix.
The date of birth has a mandatory format(dd/MM/YYYY) or can be left empty, in wich case it gets automatically saved as 01/01/1970.

Contacts can be sorted after Name, Surname or Telephone number.
Contacts can, in the same time, be filtered after an input string from the keyboard.
Filtering is also made after:
- FIX or MOBIL telephone numbers
- Contacts with a birthday today or coming soon

At startup the application notifies you if there are contacts in your list that have a birthday on the current day.
You can register the app by going to the Help -> Register menu and input the code 1987. (this will eliminate the commercials) 
The registration is marked with a "flag" file registered.txt created in the moment when the code is validated.
The app remains registered until the registered.txt file is deleted "by hand".

The app uses a db.txt file that is created, if not present, at the startup of the application.
This file is in csv format, with a ',' as separator.
In this file the application Adds and Edits contacts and from this file it will Read or Delete.

The user can export and import the contacts present in db.txt in any other .txt file by going to File -> Exporta, Importa
For Import the .txt file must be valid. Must have the same csv format and the same numer of fields for every entry.
 